import { Some } from "./Some";

describe("Test", () => {
  test("test text output", async () => {
    const some = new Some();
    expect(some.doit("World")).toEqual("Hello World!");
  });
});
