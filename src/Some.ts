import { hello } from "@deptest1/testlib";

export class Some {
  doit(text: string): string {
    return hello(text);
  }
}
