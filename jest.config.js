module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  coverageReporters: ["cobertura", "text"],
};
