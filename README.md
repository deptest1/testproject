# testlib

This is a project using the testlib library from the package registry.

## Installation

```bash
# install dependencies
$ yarn
# this should also install the testlib dependency

# build
$ yarn build

# run something with dependency
$ yarn start
# should result in 'Hello World!' on console

# run tests incl. test coverage
$ yarn test

# update vscode config (requires VSCode restart)
yarn dlx @yarnpkg/pnpify --sdk vscode

# run prettier (format all files), prettier runs automatically before commit
yarn prettier

# run eslint (find problems and fix the obvious)
yarn eslint --fix src

# update testlib dependency from private repository
$ yarn up testlib

# link testlib to a local folder
# requires yarn build in source folder to work (you can use `yarn watch`)
$ yarn link ../testlib
# This also creates an "resolutions" entry in package.json, That needs to
# be removed before committed.
```

# TODO:

- Es sollte der Typische Entwicklungsablauf für einzelne Bibliotheken dokumentiert werden
- und dann die Erweiterung, wenn man mehr als eine Bibliothek gleichzeitig benutzt, bzw. wenn man das gerade so macht, dass man die Schnittstelle baut. (Eigentlich ist es kein gutes Zeichen, wenn man das zu oft machen muss - dann ist das nicht gut geschnitten.)

# Überblick

Dieses Projekt demonstriert wie man mehrere Projekte die voneinander abhängen in einer gitlab gruppe organisieren kann. Dabei ist die testlib eine Bibliothek, die von testproject benutzt wird. Dazu wurden in diesem Projekt automatische Tests und Code Coverage ermittelt sowie eine automatische Versionierung inkl. changelog.

Für beide Projekte wurden build pipelines angelegt, die die Projekte vollständig bauen und auch in die private gitlab package registry publizieren. Eine neue Version der testlib Bibliothek löst dabei auch die Pipeline für testproject aus, sodass bei einer Änderung von testlib auch automatisch eine neue Version von testproject gebaut wird.

## Ein typischer Ablauf mit ein paar Anmerkungen:

In diesem hypothetischen Szenario gibt es einen Auftraggeber und zwei ProgrammiererInnen, die eine Aufgabe in dem verteilten Projekt erledigen.

Der Auftraggeber oder Project Owner wünscht sich eine neue Funktion in Test project, die inhaltlich in der Testlib untergebracht werden sollte. Dafür werden zwei Issues erstellt, eines für das Schaffen der Funktionalität in testlib und ein Issue für die Integration der neuen Funktion in testproject.

![Issue 1 - testlib](/uploads/cb1fa9b6363eaf695b72729aa13fab3a/Bildschirmfoto_2021-05-03_um_19.16.23.png)

![Issue 2 - testproject](/uploads/2207137b03e6669c718ad0772d5bedd2/Bildschirmfoto_2021-05-03_um_19.21.37.png)

In den Issues ist der Umfang und auch eine Zeitschätzug des Auftraggebers enthalten. Es gilt der Grundsatz, dass ohne Rücksprache von den Zeitschätzungen maximal 25% nach oben abgewichen werden darf. Wenn mehr als 25% der geschätzten Zeit benötigt wird ist eine Rückfrage zwingend erforderlich. Es geht darum inhaltliche Missverständnisse hinsichtlich des Umfangs auszuschließen und dann ggf. den Umfang gemeinsam mit neuen Erkenntnissen neu zu schätzen.

Die Entwicklerin Europa bekommt den Issue 1 aus testlib zugewiesen. Jedes Feature soll erstmal in einem Feature-Branch entwickelt werden und erst wenn es fertig ist über einen MergeRequest inkl. CodeReview in den Master überführt werden.

Dazu erzeugt Europa einen Branch aus dem Issue:

![Branch from Issue](/uploads/1a518a398767d2c6e087b8ac8db4645d/Bildschirmfoto_2021-05-03_um_19.41.13.png)

Sofern noch nicht geschehen cloned Europa das Repository und wechselt auf den Branch:

```
git clone https://gitlab.com/deptest1/testlib.git
git checkout 1-create-a-function-hi
```

Jetzt setzt die Entwicklerin die Funktion um:

```index.ts
...
export function hi(name: string): string {
  return `Hi ${name}!`;
}
```

Sie führt nun die Tests aus um zu überprüfen, ob sie eine bestehende Funktionalität kaputt gemacht hat.

```
mpoehler@marcos-macbook testlib % yarn test
 PASS  src/all.spec.ts
  Test
    ✓ test text output (2 ms)

----------|---------|----------|---------|---------|-------------------
File      | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
----------|---------|----------|---------|---------|-------------------
All files |      75 |      100 |      50 |      75 |
 index.ts |      75 |      100 |      50 |      75 | 6
----------|---------|----------|---------|---------|-------------------
Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        0.533 s, estimated 1 s
Ran all test suites.
```

Das ist nicht der Fall, ABER ihr fällt auf, dass sie für ihre Funktion noch keinen Test geschrieben hat. Das holt sie sofort nach...

```all.spec.ts
...
describe("Test", () => {
  test("test text output", async () => {
    expect(hi("World")).toEqual("Hi World!");
  });
});
```

Und danach läuft auch der Test inkl. kompletter Code-Coverage.

```
mpoehler@marcos-macbook testlib % yarn test
 PASS  src/all.spec.ts
  Test
    ✓ test text output (2 ms)
    ✓ test text output

----------|---------|----------|---------|---------|-------------------
File      | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
----------|---------|----------|---------|---------|-------------------
All files |     100 |      100 |     100 |     100 |
 index.ts |     100 |      100 |     100 |     100 |
----------|---------|----------|---------|---------|-------------------
Test Suites: 1 passed, 1 total
Tests:       2 passed, 2 total
Snapshots:   0 total
Time:        1.201 s
Ran all test suites.
```

Sie ist fertig. Nun kann sie den Code committen. Dafür muss sie ein bestimmtes Format in den commit messages benutzen, damit damit später ein schönes Changelog erzeugt werden kann.

Dabei folgenden die Commits dieser Form:

```
Tag: Short description (fixes #1234)

Longer description here if necessary
```

Where Tag can be:
**Fix** - for a bug fix.
**Update** - either for a backwards-compatible enhancement or for a rule **change** that adds reported problems.
**New** - implemented a new feature.
**Breaking** - for a backwards-incompatible enhancement or feature.
**Docs** - changes to documentation only.
**Build** - changes to build process only.
**Upgrade** - for a dependency upgrade.
**Chore** - for refactoring, adding tests, etc. (anything that isn't user-facing).

from https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-eslint

Sie macht also folgendes:

```
git add -A
git commit -m "New: added function hi()"
```

In diesem Moment starten ein lokale pre commit hook und führt prettier, linter und tests aus. Außerdem wird das Format der commit messsage überprüft. Das ist wichtig, weil die so in das Changelog eingeht.

Dann pushed sie den Branch in das remote repository...

```
git push
```

Und nun beginnt die Pipeline ihre Arbeit auf dem Feature Branch:

![Start Pipeline](/uploads/9578add5f8f29e2fd78ebd513b752501/Bildschirmfoto_2021-05-03_um_19.47.45.png)

In der Pipeline werden alle Tests abgefahren und nach einiger Zeit sieht das dann hoffentlich so aus:

![Successful Pipeline](/uploads/24526ab0e8d20523b6f917dd3b387530/Bildschirmfoto_2021-05-03_um_19.50.17.png)

Da hier alles okay ist, kann Europa nun die benötigte Zeit in den Issue eintragen (einfach einen Kommentar mit `/spend 25m` zum Issue hinzufügen) und für das Feature einen MergeRequest stellen.

![MergeRequest](/uploads/556fbc66b00c6166934fd3fa1d170d33/Bildschirmfoto_2021-05-03_um_19.53.59.png)

Der Project Owner können nun den MergeRequest beurteilen und wenn alles okay ist annehmen. Dann wird der Feature-Branch in den Main-Branch gemerged. Dann startet die
CI-Pipeline erneut, baut und testen wieder alles und erstellt eine neue Version der testlib in der gitlab package registry. Außerdem wird danach automatisch die Pipeline für testproject ausgelöst und ggf. auch dort eine neue Version erstellt.

![Start Pipeline main](/uploads/c8ac5b6289e09d767a77956e8443609c/Bildschirmfoto_2021-05-03_um_19.57.50.png)

Eine neue Version wurde angelegt, weil wir mit "New:..." in der Commit-Message eine neue Funktionalität angekündigt haben und deshalb automatisch ein Versionswechsel erfolgte.

![New Version created](/uploads/6d05fff757680574a375b98ddd9ae957/Bildschirmfoto_2021-05-03_um_19.59.07.png)

Die neue Version ist auch über die Gitlab Package Registry verfügbar:

![Package Registry](/uploads/fa5ab7406ea1d103af77f250f800deba/Bildschirmfoto_2021-05-03_um_20.01.26.png)

Dann wurde ja auch noch die Downstream pipeline im Project testproject ausgelöst:

![Downstream Pipeline](/uploads/5b26aaeaf836431ac7910957b92f8c07/Bildschirmfoto_2021-05-03_um_20.03.05.png)

Hier würde man dann auch sehen, wenn man in testproject benötigte Funktionalitäten kaputt gemacht hätte.

Für die Integration der Funktion in testproject müssen analog die folgenden Schritte durchgeführt werden:

- branch in Issue anlegen
- branch auschecken
- Änderungen durch führen
- Tests ausführen
- Tests/Implementierung ergänzen bis alles klappt
- Änderungen committen
- Pipelinelauf abwarten
- benötigte Zeit eintragen
- Merge Request stellen
- PO nimmt MergeRequest an
